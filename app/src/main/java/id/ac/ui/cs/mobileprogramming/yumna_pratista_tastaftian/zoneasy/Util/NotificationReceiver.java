package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.View.LihatRencanaActivity;

public class NotificationReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("HHEE","MASUK");
        NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent repeating_in = new Intent(context, LihatRencanaActivity.class);
        repeating_in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pIntent= PendingIntent.getActivity(context,100,repeating_in,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentIntent(pIntent)
                .setContentTitle("Jangan lupa hari H pendaftaran!")
                .setSmallIcon(android.R.drawable.arrow_up_float)
                .setContentText("3 Desember adalah hari besar anak anda!")
                .setAutoCancel(true);

        nm.notify(100, builder.build());
    }
}
