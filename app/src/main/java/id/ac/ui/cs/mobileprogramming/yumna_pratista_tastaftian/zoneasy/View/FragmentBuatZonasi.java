package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.View;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import java.util.ArrayList;
import java.util.List;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.Daerah;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.RencanaPendaftaran;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.Sekolah;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.R;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.ViewModel.DaerahViewModel;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.ViewModel.RencanaPendaftaranViewModel;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.ViewModel.SekolahViewModel;

public class FragmentBuatZonasi extends Fragment {
    private SekolahViewModel svm;
    private DaerahViewModel dvm;
    private RencanaPendaftaranViewModel rpvm;
    private Spinner daerah;
    private Spinner sekolah;
    protected ArrayAdapter daerahAdapter;
    protected ArrayAdapter sekolahAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_buat_rencana_zonasi, container, false);
        rpvm = new ViewModelProvider(this).get(RencanaPendaftaranViewModel.class);
        dvm = new ViewModelProvider(this).get(DaerahViewModel.class);
        svm = new ViewModelProvider(this).get(SekolahViewModel.class);
        daerah = v.findViewById(R.id.nama_daerah_zonasi);
        sekolah = v.findViewById(R.id.nama_sekolah_zonasi);
        fillView(v);
        showContacts(v);
        return v;
    }

    private void fillView(View v){
        new FillDataDaerah(dvm, v.getContext()).execute();
        daerah.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getContext(), "INI BRE " + daerah.getItemAtPosition(i).toString(), Toast.LENGTH_LONG).show();
                new FillDataSekolah(svm,getContext(),daerah.getItemAtPosition(i).toString()).execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        Button hitung = v.findViewById(R.id.hitung_poin_zonasi);
        final EditText jarak = v.findViewById(R.id.jarak_zonasi);
        TextView poin = v.findViewById(R.id.poin_zonasi);
        TextView nama = v.findViewById(R.id.nama_siswa);
        hitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int int_jarak = Integer.parseInt(jarak.getText().toString());
                String daerah_fix = daerah.getSelectedItem().toString();
                String sekolah_fix = sekolah.getSelectedItem().toString();
                String namaSiswa = nama.getText().toString();
                RencanaPendaftaran new_rp = new RencanaPendaftaran(namaSiswa,"Jalur zonasi", daerah_fix, sekolah_fix, int_jarak, hitungZonasi(int_jarak));
                poin.setText(Integer.toString(hitungZonasi(int_jarak)));
                new SaveRencana(rpvm, getContext()).execute(new_rp);
            }
        });
    }

    private void showContacts(View v) {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 100);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method

        } else {
            // Android version is lesser than 6.0 or the permission is already granted.
            List<String> contacts = getContactNames();
            AutoCompleteTextView act = (AutoCompleteTextView) v.findViewById(R.id.nama_siswa);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(v.getContext(),
                    android.R.layout.simple_list_item_1,
                    contacts);
            act.setAdapter(adapter);
            act.setThreshold(2);
        }
    }

    private List<String> getContactNames() {
        List<String> contacts = new ArrayList<>();
        // Get the ContentResolver
        ContentResolver cr = getActivity().getContentResolver();
        // Get the Cursor of all the contacts
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        // Move the cursor to first. Also check whether the cursor is empty or not.
        if (cursor.moveToFirst()) {
            // Iterate through the cursor
            do {
                // Get the contacts name
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if(name != null){
                    Log.i("NAME",name);
                    contacts.add(name);
                }

            } while (cursor.moveToNext());
        }
        cursor.close();

        return contacts;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts(getView());
            } else {
                Toast.makeText(getActivity(), "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private int hitungZonasi(int jarak){
        if((0 <= jarak) && (jarak <=1000)) return 400;
        else if((1001 <= jarak) && (jarak <=2000)) return 395;
        else if((2001 <= jarak) && (jarak <=3000)) return 390;
        else if((3001 <= jarak) && (jarak <=4000)) return 385;
        else if((4001 <= jarak) && (jarak <=5000)) return 380;
        else if((5001 <= jarak) && (jarak <=6000)) return 375;
        else if((6001 <= jarak) && (jarak <=7000)) return 370;
        else if((7001 <= jarak) && (jarak <=8000)) return 365;
        else if((8001 <= jarak) && (jarak <=9000)) return 360;
        else if((9001 <= jarak) && (jarak <=10000)) return 355;
        else return 350;
    }

    private class SaveRencana extends AsyncTask<RencanaPendaftaran, Void, Void>{
        private RencanaPendaftaranViewModel prvm;
        private Context context;

        SaveRencana(RencanaPendaftaranViewModel prvm, Context context){
            this.prvm = prvm;
            this.context = context;
        }

        @Override
        protected Void doInBackground(RencanaPendaftaran... rencanas) {
            RencanaPendaftaran rp = rencanas[0];
            prvm.insert(rp);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(context, "RENCANA SAVED", Toast.LENGTH_SHORT).show();
        }
    }


    private class FillDataDaerah extends AsyncTask<Void, Void, List<Daerah>>{
        private DaerahViewModel dvm;
        private Context context;

        FillDataDaerah(DaerahViewModel dvm, Context context){
            this.dvm = dvm;
            this.context = context;
        }

        @Override
        protected List<Daerah> doInBackground(Void... voids) {
            return dvm.getAllDaerahNoLive();
        }

        @Override
        protected void onPostExecute(List<Daerah> daerahs) {
            super.onPostExecute(daerahs);
            daerahAdapter = new ArrayAdapter(context, android.R.layout.simple_list_item_1, daerahs);
            daerah.setAdapter(daerahAdapter);
        }
    }

    private class FillDataSekolah extends AsyncTask<Void, Void, List<Sekolah>>{
        private SekolahViewModel svm;
        private Context context;
        private String daerah;

        FillDataSekolah(SekolahViewModel svm, Context context, String daerah){
            this.svm = svm;
            this.context = context;
            this.daerah = daerah;
        }

        @Override
        protected List<Sekolah> doInBackground(Void... voids) {
            return svm.getSekolahByDaerah(daerah);
        }

        @Override
        protected void onPostExecute(List<Sekolah> sekolahs) {
            super.onPostExecute(sekolahs);
            sekolahAdapter = new ArrayAdapter(context, android.R.layout.simple_list_item_1, sekolahs);
            sekolah.setAdapter(sekolahAdapter);
        }
    }
}
