package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Repository;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.DAO.DaerahDAO;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.Daerah;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.ROOMDatabase.DaerahRoomDatabase;

public class DaerahRepository {
    private DaerahDAO daerahDao;
    private LiveData<List<Daerah>> allDaerah;
    private Application application;

    public DaerahRepository(Application application){
        DaerahRoomDatabase db = DaerahRoomDatabase.getDatabase(application);
        this.application = application;
        this.daerahDao = db.getDaerahDAO();
        this.allDaerah = daerahDao.getDaerahByNama();
        getDaerahByApi();
    }

    public void getDaerahByApi(){
        String url = "http://dev.farizdotid.com/api/daerahindonesia/provinsi";
        RequestQueue rq = Volley.newRequestQueue(application.getApplicationContext());
        rq.start();
        List<Daerah> daerahs = new ArrayList<>();
        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET,url,(JSONObject) null , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    // Parse JSON
                    JSONArray jsonArray = response.getJSONArray("semuaprovinsi");
                    JSONObject[] newdatas = new JSONObject[jsonArray.length()];
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jso = jsonArray.getJSONObject(i);
                        newdatas[i] = jso;
                    }

                    for(int i=0;i<newdatas.length;i++){
                        Daerah d = new Daerah(newdatas[i].getString("nama").toLowerCase());
                        insert(d);
                        Log.i("DAERAH", d.getNamaDaerah());
//                        daerahs.add(d);
                    }

                } catch(JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                // Handle rerror
            }
        });
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(10000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(jsonArrayRequest);
    }

    public List<Daerah> getAllDaerahNoLive(){
        return daerahDao.getAllDaerah();
    }

    public LiveData<List<Daerah>> getAllDaerah(){
        return this.allDaerah;
    }

    public void insert(Daerah daerah){
        new InsertAsyncTask(daerahDao).execute(daerah);
    }

    private static class InsertAsyncTask extends AsyncTask<Daerah, Void, Void>{
        private DaerahDAO mDaerahDao;

        InsertAsyncTask(DaerahDAO daerahDAO){
            mDaerahDao = daerahDAO;
        }

        @Override
        protected Void doInBackground(Daerah... daerahs) {
            Daerah in = daerahs[0];
            mDaerahDao.insert(in);
            return null;
        }
    }
}
