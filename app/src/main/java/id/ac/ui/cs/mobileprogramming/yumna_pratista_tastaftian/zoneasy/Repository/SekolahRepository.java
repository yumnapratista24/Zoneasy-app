package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.DAO.SekolahDAO;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.Sekolah;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.ROOMDatabase.SekolahRoomDatabase;

public class SekolahRepository {
    private SekolahDAO sekolahDao;
    private LiveData<List<Sekolah>> allSekolah;

    public SekolahRepository(Application application){
        SekolahRoomDatabase db = SekolahRoomDatabase.getDatabase(application);
        this.sekolahDao = db.getSekolahDAO();
        this.allSekolah = sekolahDao.getAllSekolah();
    }

    public List<Sekolah> getByDaerah(String daerah){
        return sekolahDao.getByDaerah(daerah);
    }

    public LiveData<List<Sekolah>> getAllSekolah(){
        return this.allSekolah;
    }

    public void insert(Sekolah sekolah){
        new SekolahRepository.InsertAsyncTask(sekolahDao).execute(sekolah);
    }

    private static class InsertAsyncTask extends AsyncTask<Sekolah, Void, Void> {
        private SekolahDAO sekolahDao;

        InsertAsyncTask(SekolahDAO sekolahDAO){
            this.sekolahDao = sekolahDAO;
        }

        @Override
        protected Void doInBackground(Sekolah... sekolahs) {
            sekolahDao.insert(sekolahs[0]);
            return null;
        }
    }
}
