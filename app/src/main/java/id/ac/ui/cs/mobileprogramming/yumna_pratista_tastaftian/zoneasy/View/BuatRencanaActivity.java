package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.View;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.R;

public class BuatRencanaActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buat_rencana);
        setupButton();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container_buat_rencana, new FragmentBuatZonasi());
        ft.commit();
    }

    public void setupButton(){
        Button b1 = findViewById(R.id.button_buat_zonasi);
        Button b2 = findViewById(R.id.button_buat_kombinasi);
        Button b3 = findViewById(R.id.button_buat_prestasi);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container_buat_rencana, new FragmentBuatZonasi());
                ft.commit();
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container_buat_rencana, new FragmentBuatKombinasi());
                ft.commit();
            }
        });
    }
}
