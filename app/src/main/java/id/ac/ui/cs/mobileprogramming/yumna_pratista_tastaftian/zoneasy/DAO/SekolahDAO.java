package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.DAO;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.Sekolah;

@Dao
public interface SekolahDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Sekolah... sekolahs);

    @Query("DELETE FROM sekolah_table")
    void deleteAll();

    @Query("SELECT * FROM sekolah_table")
    LiveData<List<Sekolah>> getAllSekolah();

    @Query("SELECT * FROM sekolah_table WHERE namaDaerah =:daerah")
    List<Sekolah> getByDaerah(String daerah);
}
