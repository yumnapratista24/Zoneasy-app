package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.ROOMDatabase;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.DAO.SekolahDAO;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.Daerah;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.Sekolah;

@Database(entities = {Sekolah.class, Daerah.class},version = 1, exportSchema = false)
public abstract class SekolahRoomDatabase extends RoomDatabase {
    private static volatile SekolahRoomDatabase instance;

    public static SekolahRoomDatabase getDatabase(Application application){
        if(instance == null){
            synchronized (SekolahRoomDatabase.class){
                if(instance == null){
                    instance = Room.databaseBuilder(application.getApplicationContext(),
                            SekolahRoomDatabase.class,
                            "sekolah_database")
                            .addCallback(sekolahRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return instance;
    }

    public abstract SekolahDAO getSekolahDAO();

    private static RoomDatabase.Callback sekolahRoomDatabaseCallback = new RoomDatabase.Callback(){
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            Log.i("CALLBACK","CALLED");
            new SekolahRoomDatabase.SekolahPopulate(instance).execute();
        }
    };

    private static class SekolahPopulate extends AsyncTask<Void, Void, Void> {
        private final SekolahDAO sDao;

        SekolahPopulate(SekolahRoomDatabase db){
            sDao = db.getSekolahDAO();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            sDao.deleteAll();
            Sekolah jawa_timur = new Sekolah("SMAN 1 Jatim",116,"jawa timur");
            sDao.insert(jawa_timur);
            Sekolah jawa_timur2 = new Sekolah("SMAN 2 Jatim",116,"jawa timur");
            sDao.insert(jawa_timur2);
            Sekolah jawa_timur3 = new Sekolah("SMAN 3 Jatim",116,"jawa timur");
            sDao.insert(jawa_timur3);
            Sekolah jawa_barat = new Sekolah("SMAN 1 Jabar",116,"jawa barat");
            sDao.insert(jawa_barat);
            Sekolah jakarta = new Sekolah("SMAN 1 Jakarta",116,"dki jakarta");
            sDao.insert(jakarta);
            Sekolah aceh = new Sekolah("SMAN 1 Aceh",116,"aceh");
            sDao.insert(aceh);
            Sekolah sumatera_bar = new Sekolah("SMAN 1 Sumbar",116,"sumatera barat");
            sDao.insert(sumatera_bar);
            Sekolah sumatera_ut = new Sekolah("SMAN 1 Sumut",116,"sumatera utara");
            sDao.insert(sumatera_ut);
            Sekolah riau = new Sekolah("SMAN 1 Riau",116,"riau");
            sDao.insert(riau);
            Sekolah jambi = new Sekolah("SMAN 1 Jambi",116,"jambi");
            sDao.insert(jambi);
            Sekolah sumatera_sel = new Sekolah("SMAN 1 Sumsel",116,"sumatera selatan");
            sDao.insert(sumatera_sel);
            Sekolah bengkulu = new Sekolah("SMAN 1 Bengkulu",116,"bengkulu");
            sDao.insert(bengkulu);
            Sekolah lampung = new Sekolah("SMAN 1 Lampung",116,"lampung");
            sDao.insert(lampung);
            Sekolah bangka_belitung = new Sekolah("SMAN 1 Bangka Belitung",116,"kepulauan bangka belitung");
            sDao.insert(bangka_belitung);
            Sekolah kep_riau = new Sekolah("SMAN 1 Kepulauan Riau",116,"kepulauan riau");
            sDao.insert(kep_riau);
            Sekolah jawa_tengah = new Sekolah("SMAN 1 Jakarta",116,"dki jakarta");
            sDao.insert(jawa_tengah);
            Sekolah di_yogyakarta = new Sekolah("SMAN 1 Yogyakarta",116,"di yogyakarta");
            sDao.insert(di_yogyakarta);
            Sekolah banten = new Sekolah("SMAN 1 Banten",116,"banten");
            sDao.insert(banten);
            Sekolah bali = new Sekolah("SMAN 1 Bali",116,"bali");
            sDao.insert(bali);
            Sekolah ntb = new Sekolah("SMAN 1 NTB",116,"nusa tenggara barat");
            sDao.insert(ntb);
            Sekolah ntt = new Sekolah("SMAN 1 NTT",116,"nusa tenggara timur");
            sDao.insert(ntt);
            Sekolah kal_bar = new Sekolah("SMAN 1 Kalimantan Barat",116,"kalimantan barat");
            sDao.insert(kal_bar);
            Log.i("POPULATE SEKOLAH","INSERTED");

            return null;
        }
    }
}
