package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.RencanaPendaftaran;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.R;

public class LihatRencanaAdapter extends RecyclerView.Adapter<LihatRencanaAdapter.LihatRencanaAdapaterViewHolder> {
    class LihatRencanaAdapaterViewHolder extends RecyclerView.ViewHolder{
        private final TextView namaSekolah;
        private final TextView namaJalur;
        private final TextView poin;
        private final TextView namaSiswa;

        public LihatRencanaAdapaterViewHolder(@NonNull View itemView) {
            super(itemView);
            this.namaSiswa = itemView.findViewById(R.id.nama_siswa);
            this.namaSekolah = itemView.findViewById(R.id.nama_sekolah);
            this.poin = itemView.findViewById(R.id.jumlah_poin);
            this.namaJalur = itemView.findViewById(R.id.nama_jalur);
        }
    }

    private List<RencanaPendaftaran> rencanas;
    private RecyclerView rv;
    private final LayoutInflater inflater;
    private Context context;

    public LihatRencanaAdapter(Context context, RecyclerView rv){
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.rv = rv;
    }

    public void setRencanas(List<RencanaPendaftaran> rencanas) {
        this.rencanas = rencanas;
        notifyDataSetChanged();
    }

    private void makeDialog(View view, String sekolah, String jalur_str, int poin){
        final Dialog detail = new Dialog(view.getContext());

        // Layout
        detail.setContentView(R.layout.dialog_detail);
        Button tutup = detail.findViewById(R.id.close);
        TextView nama_sekolah = detail.findViewById(R.id.nama_sekolah);
        nama_sekolah.setText(sekolah);
        TextView jalur = detail.findViewById(R.id.nama_jalur);
        jalur.setText(jalur_str);
        TextView total_poin = detail.findViewById(R.id.total_poin);
        total_poin.setText("Poin :" + Integer.toString(poin));
        tutup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detail.dismiss();
            }
        });
        detail.show();
    }

    @NonNull
    @Override
    public LihatRencanaAdapaterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View perItem = inflater.inflate(R.layout.viewholder_rencana, parent,false);
        perItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int itemPosition = rv.getChildAdapterPosition(view);
                makeDialog(perItem, rencanas.get(itemPosition).getNamaSekolah(),
                        rencanas.get(itemPosition).getJalur(),
                        rencanas.get(itemPosition).getPoin());
            }
        });
        return new LihatRencanaAdapaterViewHolder(perItem);
    }

    @Override
    public void onBindViewHolder(@NonNull LihatRencanaAdapaterViewHolder holder, int position) {
        RencanaPendaftaran obj = rencanas.get(position);
        holder.namaSiswa.setText(obj.getNamaSiswa());
        holder.namaSekolah.setText(obj.getNamaSekolah());
        holder.namaJalur.setText(obj.getJalur());
        holder.poin.setText(Integer.toString(obj.getPoin()));
    }

    @Override
    public int getItemCount() {
        if(rencanas == null) return 0;
        return rencanas.size();
    }
}
