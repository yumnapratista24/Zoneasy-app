package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.DAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.RencanaPendaftaran;

@Dao
public interface RencanaPendaftaranDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(RencanaPendaftaran... rencanaPendaftarans);

    @Query("DELETE FROM rencana_pendaftaran")
    void deleteAll();

    @Query("SELECT * FROM rencana_pendaftaran")
    List<RencanaPendaftaran> getAllRencanaPendaftaran();
}
