package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.List;

@Entity(tableName = "daerah_table")
public class Daerah {
    @PrimaryKey
    @NonNull
    private String namaDaerah;

    @Ignore
    private List<Sekolah> listSekolah;

    @Override
    public String toString() {
        return namaDaerah;
    }

    public Daerah(String namaDaerah) {
        this.namaDaerah = namaDaerah;
    }

    public String getNamaDaerah(){
        return namaDaerah;
    }

    public List<Sekolah> getAllSekolah(){
        return listSekolah;
    }

    public void setAllSekolah(List<Sekolah> sekolahs){
        listSekolah = sekolahs;
    }

}
