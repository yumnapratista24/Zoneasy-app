package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.ROOMDatabase;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.DAO.DaerahDAO;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.Daerah;

@Database(entities = {Daerah.class}, version = 1, exportSchema = false)
public abstract class DaerahRoomDatabase extends RoomDatabase {
    private static volatile DaerahRoomDatabase instance;

    public static DaerahRoomDatabase getDatabase(final Application application) {
        if(instance == null){
            Log.i("INSTANCE","NULL");
            // Sinkronisasi agar menjadi singleton
            synchronized (DaerahRoomDatabase.class){
                if(instance == null){
                    instance = Room.databaseBuilder(application.getApplicationContext(),
                            DaerahRoomDatabase.class,
                            "daerah_database")
                            .allowMainThreadQueries()
                            .addCallback(daerahRoomDatabaseCallback)
                            .build();

                    Log.i("INSTANCE","BUILD");
                }
            }
        }
        return instance;
    }

    public abstract DaerahDAO getDaerahDAO();

    private static RoomDatabase.Callback daerahRoomDatabaseCallback = new RoomDatabase.Callback(){
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            Log.i("CALLBACK","CALLED");
//            new DaerahPopulate(instance).execute();
        }
    };

//    private static class DaerahPopulate extends AsyncTask <Void, Void, Void>{
//        private final DaerahDAO dDao;
//
//        DaerahPopulate(DaerahRoomDatabase db){
//            dDao = db.getDaerahDAO();
//        }
//
//        @Override
//        protected Void doInBackground(Void... voids) {
//            dDao.deleteAll();
//            Daerah jawa_timur = new Daerah("jawa timur");
//            dDao.insert(jawa_timur);
//            Daerah jawa_barat = new Daerah("jawa barat");
//            dDao.insert(jawa_barat);
//            Daerah jakarta = new Daerah("jakarta");
//            dDao.insert(jakarta);
//            Daerah jombang = new Daerah("jombang");
//            dDao.insert(jombang);
//            Log.i("POPULATE","INSERTED");
//
//            return null;
//        }
//    }
}
