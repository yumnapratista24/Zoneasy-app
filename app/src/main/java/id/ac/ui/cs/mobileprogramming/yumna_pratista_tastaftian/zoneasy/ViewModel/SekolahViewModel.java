package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.ViewModel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.Sekolah;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Repository.SekolahRepository;

public class SekolahViewModel extends AndroidViewModel {
    private SekolahRepository dataRepo;
    private LiveData<List<Sekolah>> allSekolah;

    public SekolahViewModel(Application application) {
        super(application);
        this.dataRepo = new SekolahRepository(application);
        this.allSekolah = dataRepo.getAllSekolah();
    }

    public void insert(Sekolah sekolah){
        dataRepo.insert(sekolah);
    }

    public List<Sekolah> getSekolahByDaerah(String daerah){
        return dataRepo.getByDaerah(daerah);
    }

    public LiveData<List<Sekolah>> getAllSekolah(){
        return this.allSekolah;
    }
}
