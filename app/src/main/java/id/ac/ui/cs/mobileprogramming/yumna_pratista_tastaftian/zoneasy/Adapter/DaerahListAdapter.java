package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.Daerah;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.R;

public class DaerahListAdapter extends ArrayAdapter<Daerah> {
    private final LayoutInflater inflater;

    public DaerahListAdapter(Context context,
                             List<Daerah> daerahs){
        super(context,0, daerahs);
        this.inflater = LayoutInflater.from(context);
    }

    private static class ViewHolder{
        private TextView itemView;
    }
    @Override
    public View getView(int position,
                        View convertView,
                        ViewGroup parent) {
        // get data dari posisi
        Daerah d = getItem(position);
        convertView = inflater.inflate(R.layout.support_simple_spinner_dropdown_item, parent, false);
        TextView t = convertView.findViewById(android.R.id.text1);
        t.setText(d.getNamaDaerah());
        return convertView;
    }
}
