package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.View;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Adapter.LihatRencanaAdapter;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.RencanaPendaftaran;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.R;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.ViewModel.RencanaPendaftaranViewModel;

public class LihatRencanaActivity extends AppCompatActivity {
    private RencanaPendaftaranViewModel rpvm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_rencana);
        RecyclerView rv = findViewById(R.id.recycler_rencana);
        rpvm = new ViewModelProvider(this).get(RencanaPendaftaranViewModel.class);
        final LihatRencanaAdapter lra = new LihatRencanaAdapter(this,rv);
        rv.setAdapter(lra);
        rv.setLayoutManager(new LinearLayoutManager(this));
        new GetRencanas(rpvm,lra).execute();
    }

    private class GetRencanas extends AsyncTask<Void,Void, List<RencanaPendaftaran>> {
        private RencanaPendaftaranViewModel rpvm;
        private LihatRencanaAdapter adapter;

        GetRencanas(RencanaPendaftaranViewModel rpvm, LihatRencanaAdapter adapter){
            this.rpvm = rpvm;
            this.adapter = adapter;
        }

        @Override
        protected List<RencanaPendaftaran> doInBackground(Void... voids) {
            return rpvm.getAllRencana();
        }

        @Override
        protected void onPostExecute(List<RencanaPendaftaran> rencanas) {
            super.onPostExecute(rencanas);
            adapter.setRencanas(rencanas);
        }
    }
}
