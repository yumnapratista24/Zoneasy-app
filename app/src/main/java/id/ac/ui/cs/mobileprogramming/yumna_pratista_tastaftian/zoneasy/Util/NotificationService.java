package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import java.util.Timer;
import java.util.TimerTask;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.View.LihatRencanaActivity;

public class NotificationService extends Service {
    private Timer timer;
    private TimerTask timerTask;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(getApplicationContext(), "SERVICE START COMMAND", Toast.LENGTH_SHORT).show();
        super.onStartCommand(intent, flags, startId);

        start_timer();

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    final Handler handler = new Handler();

    public void start_timer(){
        timer = new Timer();

        initializeTimerTask();

        timer.schedule(timerTask, 5000, 5 * 1000);

    }

    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {

                        Log.i("HHEE","MASUK");
                        Toast.makeText(getApplicationContext(), "MASUKKK", Toast.LENGTH_LONG).show();
                        NotificationManager nm = (NotificationManager)getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                        Intent repeating_in = new Intent(getApplicationContext(), LihatRencanaActivity.class);
                        repeating_in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        PendingIntent pIntent= PendingIntent.getActivity(getApplicationContext(),100,repeating_in,PendingIntent.FLAG_UPDATE_CURRENT);

                        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext())
                                .setContentIntent(pIntent)
                                .setContentTitle("Jangan lupa!")
                                .setSmallIcon(android.R.drawable.arrow_up_float)
                                .setContentText("HEHEHHEE")
                                .setAutoCancel(true);

                        nm.notify(100, builder.build());


                    }
                });
            }
        };
    }
}
