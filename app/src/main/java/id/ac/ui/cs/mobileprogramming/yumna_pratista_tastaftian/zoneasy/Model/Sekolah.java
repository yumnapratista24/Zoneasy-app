package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "sekolah_table")
public class Sekolah {
    @PrimaryKey
    @NonNull
    private String namaSekolah;
    private int jumlahSiswaMax;
    private final String namaDaerah;

    public Sekolah(String namaSekolah, int jumlahSiswaMax, final String namaDaerah) {
        this.namaSekolah = namaSekolah;
        this.jumlahSiswaMax = jumlahSiswaMax;
        this.namaDaerah = namaDaerah;
    }

    @Override
    public String toString() {
        return namaSekolah;
    }

    public String getNamaDaerah(){
        return namaDaerah;
    }

    public String getNamaSekolah() {
        return namaSekolah;
    }

    public int getJumlahSiswaMax() {
        return jumlahSiswaMax;
    }
}
