package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "rencana_pendaftaran")
public class RencanaPendaftaran {
    @PrimaryKey(autoGenerate = true)
    private int rencanaId;
    private String namaDaerah;
    private String namaSekolah;
    private int jarak;
    private int poin;
    private String jalur;
    private String namaSiswa;

    public RencanaPendaftaran(String namaSiswa, String jalur, String namaDaerah, String namaSekolah, int jarak, int poin) {
        this.namaSiswa = namaSiswa;
        this.namaDaerah = namaDaerah;
        this.namaSekolah = namaSekolah;
        this.jarak = jarak;
        this.poin = poin;
        this.jalur = jalur;
    }

    public String getNamaSiswa(){return this.namaSiswa;}

    public String getJalur(){
        return this.jalur;
    }

    public int getRencanaId() {
        return rencanaId;
    }

    public void setRencanaId(int rencanaId) {
        this.rencanaId = rencanaId;
    }

    public String getNamaDaerah() {
        return namaDaerah;
    }

    public void setNamaDaerah(String namaDaerah) {
        this.namaDaerah = namaDaerah;
    }

    public String getNamaSekolah() {
        return namaSekolah;
    }

    public void setNamaSekolah(String namaSekolah) {
        this.namaSekolah = namaSekolah;
    }

    public int getJarak() {
        return jarak;
    }

    public void setJarak(int jarak) {
        this.jarak = jarak;
    }

    public int getPoin() {
        return poin;
    }

    public void setPoin(int poin) {
        this.poin = poin;
    }
}
