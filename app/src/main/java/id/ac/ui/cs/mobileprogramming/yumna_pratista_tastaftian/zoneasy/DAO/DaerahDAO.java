package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.DAO;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.Daerah;

@Dao
public interface DaerahDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Daerah... daerahs);

    @Query("DELETE FROM daerah_table")
    void deleteAll();

    @Query("SELECT * FROM daerah_table")
    LiveData<List<Daerah>> getDaerahByNama();

    @Query("SELECT * FROM daerah_table")
    List<Daerah> getAllDaerah();
}
