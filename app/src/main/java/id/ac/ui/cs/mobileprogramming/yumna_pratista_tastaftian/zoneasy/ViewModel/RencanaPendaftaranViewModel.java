package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.ViewModel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.RencanaPendaftaran;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Repository.RencanaPendaftaranRepository;

public class RencanaPendaftaranViewModel extends AndroidViewModel {
    private RencanaPendaftaranRepository dataRepo;
    private List<RencanaPendaftaran> allRencana;

    public RencanaPendaftaranViewModel(Application application) {
        super(application);
        this.dataRepo = new RencanaPendaftaranRepository(application);
        this.allRencana= dataRepo.getAllRencana();
    }

    public void insert(RencanaPendaftaran rencanaPendaftaran){
        dataRepo.insert(rencanaPendaftaran);
    }

    public List<RencanaPendaftaran> getAllRencana(){
        return dataRepo.getAllRencana();
    }
}
