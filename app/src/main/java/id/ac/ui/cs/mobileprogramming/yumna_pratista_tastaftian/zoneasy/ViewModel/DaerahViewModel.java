package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.ViewModel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.Daerah;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Repository.DaerahRepository;

public class DaerahViewModel extends AndroidViewModel {
    private DaerahRepository dataRepo;
    private LiveData<List<Daerah>> allDaerah;

    public DaerahViewModel(Application application) {
        super(application);
        this.dataRepo = new DaerahRepository(application);
        this.allDaerah = dataRepo.getAllDaerah();
    }

    public void insert(Daerah daerah){
        dataRepo.insert(daerah);
    }

    public LiveData<List<Daerah>> getAllDaerah(){
        return this.allDaerah;
    }

    public List<Daerah> getAllDaerahNoLive(){
        return dataRepo.getAllDaerahNoLive();
    }
}
