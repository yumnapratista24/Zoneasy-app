package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.util.Calendar;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.R;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Util.NotificationReceiver;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.ViewModel.DaerahViewModel;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.ViewModel.RencanaPendaftaranViewModel;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.ViewModel.SekolahViewModel;

public class MainActivity extends AppCompatActivity {

    private SekolahViewModel dvm;
    private RencanaPendaftaranViewModel rpvm;
    private DaerahViewModel dvd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupPage(getApplicationContext());
        startNotification(getApplicationContext());
    }

    private void setupPage(Context context){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        // menaruh fragment gambar
        rpvm = new ViewModelProvider(this).get(RencanaPendaftaranViewModel.class);
        dvm = new ViewModelProvider(this).get(SekolahViewModel.class);
        dvd = new ViewModelProvider(this).get(DaerahViewModel.class);
        ft.replace(R.id.fragment_container_utama_1, new FragmentUtama1());
        ft.replace(R.id.fragment_container_utama_2, new FragmentUtama2());
        ft.commit();
        startNotification(context);
    }

    private void startNotification(Context context){
        Calendar c = Calendar.getInstance();

        Intent intent = new Intent(context, NotificationReceiver.class);
        PendingIntent pIntent = PendingIntent.getBroadcast(context, 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis() + 5000,1000, pIntent);
    }
}
