package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.ROOMDatabase;

import android.app.Application;
import android.util.Log;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.DAO.RencanaPendaftaranDAO;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.RencanaPendaftaran;

@Database(entities = {RencanaPendaftaran.class},version = 1, exportSchema = false)
public abstract class RencanaPendaftaranRoomDatabase extends RoomDatabase {
    private static volatile RencanaPendaftaranRoomDatabase instance;

    public static RencanaPendaftaranRoomDatabase getDatabase(final Application application) {
        if(instance == null){
            // Sinkronisasi agar menjadi singleton
            synchronized (RencanaPendaftaranRoomDatabase.class){
                if(instance == null){
                    instance = Room.databaseBuilder(application.getApplicationContext(),
                            RencanaPendaftaranRoomDatabase.class,
                            "rencana_pendaftaran_database")
                            .allowMainThreadQueries()
                            .build();

                    Log.i("INSTANCE","BUILD");
                }
            }
        }
        return instance;
    }

    public abstract RencanaPendaftaranDAO getRencanaPendaftaranDAO();

}
