package id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Repository;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.DAO.RencanaPendaftaranDAO;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.Model.RencanaPendaftaran;
import id.ac.ui.cs.mobileprogramming.yumna_pratista_tastaftian.zoneasy.ROOMDatabase.RencanaPendaftaranRoomDatabase;

public class RencanaPendaftaranRepository {
    private RencanaPendaftaranDAO rencanaPendaftaranDao;
    private List<RencanaPendaftaran> allRencana;

    public RencanaPendaftaranRepository(Application application){
        RencanaPendaftaranRoomDatabase db = RencanaPendaftaranRoomDatabase.getDatabase(application);
        this.rencanaPendaftaranDao = db.getRencanaPendaftaranDAO();
        this.allRencana = rencanaPendaftaranDao.getAllRencanaPendaftaran();
    }

    public List<RencanaPendaftaran> getAllRencana(){
        return rencanaPendaftaranDao.getAllRencanaPendaftaran();
    }

    public void insert(RencanaPendaftaran rencanaPendaftaran){
        new InsertAsyncTask(rencanaPendaftaranDao).execute(rencanaPendaftaran);
    }

    private static class InsertAsyncTask extends AsyncTask<RencanaPendaftaran, Void, Void> {
        private RencanaPendaftaranDAO mrencanaDao;

        InsertAsyncTask(RencanaPendaftaranDAO rencanaDAO){
            mrencanaDao = rencanaDAO;
        }

        @Override
        protected Void doInBackground(RencanaPendaftaran... rencanas) {
            mrencanaDao.insert(rencanas[0]);
            return null;
        }
    }

}
